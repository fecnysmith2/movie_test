import React from 'react';

import "../../../assets/styles/components/utils/MovieModal.scss";
import {CardContent, CardMedia, Container, Modal, Typography} from "@material-ui/core";
import Backdrop from "@material-ui/core/Backdrop/Backdrop";
import Fade from "@material-ui/core/Fade/Fade";
import {connect} from "react-redux";
import {closeDetails} from "../../../redux/actions/moviesActions";
import Card from "@material-ui/core/Card/Card";

const MovieModalIndex = (prop: any) => {
    const visible = !!prop.movieDetailsOpened;

    return (
        <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            open={visible}
            className="moviemodal-wrapper"
            closeAfterTransition
            onClose={prop.closeDetails}
            BackdropComponent={Backdrop}
            BackdropProps={{
                timeout: 500,
            }}
        >
            <Fade in={visible}>
                <Container maxWidth="lg">
                    {prop.movieDetailsOpened && <Card className={'details'}>

                        <CardMedia
                            className="poster"
                            image={prop.movieDetailsOpened.imdb.Poster}
                            title={prop.movieDetailsOpened.imdb.Title}
                        />
                        <div className={'content'}>
                            <CardContent className={''}>
                                <Typography component="h5" variant="h3">
                                    {prop.movieDetailsOpened.imdb.Title}
                                </Typography>
                                <Typography variant="subtitle1" color="textSecondary">
                                    {prop.movieDetailsOpened.imdb.Year}
                                </Typography>
                                <Typography variant="body2" color="textSecondary" className="description">
                                    <div
                                        dangerouslySetInnerHTML={{
                                            __html: prop.movieDetailsOpened.wikipedia.extract
                                        }}/>
                                </Typography>
                            </CardContent>
                        </div>
                    </Card>}
                </Container>
            </Fade>
        </Modal>
    );
};


const mapStateToProps = (state: any) => {
    return {
        movieDetailsOpened: state.moviesReducer.movieDetailsOpened
    };
};

const mapDispatchToProps = (dispatch: any) => {
    return {
        closeDetails: () => dispatch(closeDetails())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(MovieModalIndex);