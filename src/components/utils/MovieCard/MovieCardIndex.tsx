import React from 'react';

import "../../../assets/styles/components/utils/MovieCard.scss";
import Card from "@material-ui/core/Card/Card";
import {
    Button, CardActionArea, CardContent, CardMedia, CircularProgress, Link,
    Typography
} from "@material-ui/core";
import CardActions from "@material-ui/core/CardActions/CardActions";
import {fetchWikipedia, openDetails} from "../../../redux/actions/moviesActions";
import {connect} from "react-redux";


const MovieCardIndex = (prop: {movie: any, openDetails: Function}) => {
    const [wikipediaData, setWikipediaData] = React.useState();


    React.useEffect(() => {
        fetchWikipedia(prop.movie.Title).then(setWikipediaData)
    }, [prop]);

    const cutTitle = (title: String) => {
        if(title.length > 30)
            return title.substr(0,30) + "...";
        return title;
    };
    return (
        <Card className="moviecard-wrapper">
            <CardActionArea onClick={() => prop.openDetails(prop.movie, wikipediaData)}>
                <CardMedia
                    className="moviecard-poster"
                    image={prop.movie.Poster}
                    title={prop.movie.Title}
                />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="h4" className="title">
                        {cutTitle(prop.movie.Title)}
                    </Typography>
                    <Typography gutterBottom variant="body1" component="h4">
                        ({prop.movie.Year})
                    </Typography>
                    <Typography variant="body2" color="textSecondary" component="div" className="description">
                        {!wikipediaData && <CircularProgress size={16} />}
                        {wikipediaData && <div>1</div>}
                    </Typography>
                </CardContent>
            </CardActionArea>
            <CardActions>
                <Link href={`https://www.imdb.com/title/${prop.movie.imdbID}`} target="_blank">
                    <Button size="small" color="primary">
                    Imdb
                    </Button>
                </Link>
                {wikipediaData && wikipediaData.pageid && <Link href={`https://en.wikipedia.org/?curid=${wikipediaData.pageid}`} target="_blank">
                    <Button size="small" color="primary">
                    Wiki
                    </Button>
                </Link>}
            </CardActions>
        </Card>
    );
};


const mapDispatchToProps = (dispatch: any) => {
    return {
        openDetails: (imdbData: any, wikipediaData: any) => dispatch(openDetails(imdbData, wikipediaData))
    };
};

export default connect(null, mapDispatchToProps)(MovieCardIndex);
