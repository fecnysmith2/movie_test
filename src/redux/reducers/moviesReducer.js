const initialState = {
    searching: false,
    title: '',
    movies: [],
    totalResults: 0,
    movieDetailsOpened: null
};

const moviesReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'SET_SEARCHING': {
            return {
                ...state,
                searching: action.searching
            }
        }
        case 'OPEN_DETAILS': {
            return {
                ...state,
                movieDetailsOpened: {imdb: action.imdbData, wikipedia: action.wikipediaData}
            }
        }
        case 'CLOSE_DETAILS': {
            return {
                ...state,
                movieDetailsOpened: null
            }
        }
        case 'SET_MOVIES': {
            return {
                ...state,
                movies: action.movies,
                title: action.title,
                searching: false,
                totalResults: action.totalResults
            }
        }
        default: {
            return state;
        }
    }
};

// Exports
export default moviesReducer;