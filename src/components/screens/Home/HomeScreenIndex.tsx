import React from 'react';
import {connect} from "react-redux";

import "../../../assets/styles/components/screens/HomeScreen.scss";
import SearchPanelIndex from "../../utils/SearchPanel/SearchPanelIndex";
import {Container, Typography} from "@material-ui/core";

const HomeScreenIndex = (prop: { totalResults: Number, searching: Boolean }|any) => {
    console.log(prop);
    return (
        <div className={`homescreen-wrapper ${parseInt(prop.totalResults) > 0 || prop.searching ? 'active' : ''}`}>
            <div className="homescreen-bg"/>
            <Container maxWidth="sm" className="homescreen-container">

                <Typography variant="h4" gutterBottom>
                    Movie Search
                </Typography>

                <SearchPanelIndex />
            </Container>
        </div>
    );
};


const mapStateToProps = (state: any) => {
    return {
        totalResults: state.moviesReducer.totalResults,
        searching: state.moviesReducer.searching
    };
};

export default connect(mapStateToProps, null)(HomeScreenIndex);