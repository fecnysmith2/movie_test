const wikiSearchURL = "https://en.wikipedia.org/w/api.php?origin=%2A&action=query&prop=description%7Cinfo%7Cextracts&format=json&eititle=Template:Infobox_film&titles=";
const omdbSearchURL = "http://www.omdbapi.com/?apikey=39a95135&plot=full&s=";

export const openDetails = (imdbData: any, wikipediaData: any)=> ({
    type: 'OPEN_DETAILS',
    imdbData: imdbData,
    wikipediaData: wikipediaData,
});

export const closeDetails = ()=> ({
    type: 'CLOSE_DETAILS',
});

export const setSearching = (searching: Boolean)=> ({
    type: 'SET_SEARCHING',
    searching: searching
});

export const setMovies = (title: String, movies: any, totalResults: Number)=> ({
    type: 'SET_MOVIES',
    movies: movies,
    title: title,
    totalResults: totalResults
});

export const fetchMovies = (title: String) => {

    return function(dispatch: any) {
        setSearching(true);
        return fetch(omdbSearchURL + title)
            .then((response: any) => {
                return response.json();
            })
            .then(async (response: any) => {
                const {Search, totalResults} = response;
                setTimeout(() => dispatch(setMovies(title, Search || [], totalResults)), 2000);
            }).finally(() => {
                setSearching(false);
            });
    };
};

export const fetchWikipedia = (title: String) => {

    return fetch(wikiSearchURL + title)
        .then((response: any) => {
            return response.json();
        })
        .then(async (response: any) => {
            if(response.query && response.query.pages){
                return Object.entries(response.query.pages)[0][1];
            }
            return null;
        });
};