import React from 'react';

import "../../../assets/styles/components/utils/SearchPanel.scss";
import {IconButton, Icon, InputBase} from "@material-ui/core";
import Paper from "@material-ui/core/Paper/Paper";

import {connect} from "react-redux";
import {fetchMovies, setSearching} from "../../../redux/actions/moviesActions";

const SearchPanelIndex = (prop: { fetchMovies: Function, setSearching: Function, totalResults: Number, title: String }) => {
    const searchInput = React.useRef({value: prop.title});

    const searchMovie = async (event: React.FormEvent<HTMLDivElement>) => {
        event.preventDefault(); // To prevent form submit

        prop.setSearching();
        prop.fetchMovies(searchInput.current.value);

        return;
    };

    return (
        <Paper component="form" className={`searchpanel-wrapper`} onSubmit={searchMovie}>
            <InputBase
                fullWidth
                id="searchfield"
                defaultValue={prop.title}
                inputRef={searchInput}
                className={"searchpanel-input"}
                placeholder="Search for movie title"
                inputProps={{'aria-label': 'Search for movie title'}}
            />
            <IconButton type="submit" aria-label="search">
                <Icon fontSize="large">search</Icon>
            </IconButton>
        </Paper>
    );
};

const mapStateToProps = (state: any) => {
    return {
        totalResults: state.moviesReducer.totalResults,
        title: state.moviesReducer.title
    };
};

const mapDispatchToProps = (dispatch: any) => {
    return {
        fetchMovies: (title: String) => dispatch(fetchMovies(title)),
        setSearching: () => dispatch(setSearching(true)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchPanelIndex);
