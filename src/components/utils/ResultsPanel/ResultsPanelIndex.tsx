import React from 'react';

import "../../../assets/styles/components/utils/ResultsPanel.scss";
import {connect} from "react-redux";
import {CircularProgress, Grid} from "@material-ui/core";
import MovieCardIndex from "../MovieCard/MovieCardIndex";
import MovieModalIndex from "../MovieModal/MovieModalIndex";

const ResultsPanelIndex = (props: any) => {
    const {movies, searching, movieDetailsOpened} = props;

    return (
    <div className="resultspanel-wrapper">
        {searching && <CircularProgress size={30} className="spinner"/>}

        {!searching && <Grid container direction="row" justify="center" alignItems="center" spacing={2}>
            {movies && movies.map((movieData: any, index: Number) => <Grid item lg={2} md={4} sm={6} xs={12} key={`movieresult-${index}`}>
                <MovieCardIndex movie={movieData} />
            </Grid>)}
        </Grid>}

        <MovieModalIndex/>
    </div>
    );
};

const mapStateToProps = (state: any) => {
    return {
        searching: state.moviesReducer.searching,
        totalResults: state.moviesReducer.totalResults,
        movies: state.moviesReducer.movies,
        movieDetailsOpened: state.moviesReducer.movieDetailsOpened
    };
};

export default connect(mapStateToProps, null)(ResultsPanelIndex);
