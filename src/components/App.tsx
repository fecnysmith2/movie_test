import React from 'react';
import HomeScreenIndex from "./screens/Home/HomeScreenIndex";
import ResultsPanelIndex from "./utils/ResultsPanel/ResultsPanelIndex";

const App = () => {
    return (
        <div>
            <HomeScreenIndex />
            <ResultsPanelIndex />
        </div>
    );
};

export default App;
