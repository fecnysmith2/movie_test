// Imports: Dependencies
import { combineReducers } from 'redux';

// Imports: Reducers
import moviesReducer from "./moviesReducer";

// Redux: Root Reducer
const rootReducer = combineReducers({
    moviesReducer: moviesReducer
});

// Exports
export default rootReducer;