import {createStore, applyMiddleware} from 'redux';
import thunk from "redux-thunk";
import {persistStore, persistReducer} from 'redux-persist';
import storage from 'redux-persist/lib/storage'
import { composeWithDevTools } from 'redux-devtools-extension';

import rootReducer from '../reducers/index';

const persistConfig = {
    key: 'root',
    storage: storage,

    whitelist: [
        'moviesReducer'
    ],

    blacklist: [],
};

// Middleware: Redux Persist Persisted Reducer
const persistedReducer = persistReducer(persistConfig, rootReducer);

// Redux: Store
export const store = createStore(persistedReducer, composeWithDevTools(applyMiddleware(thunk)));
export let persistor = persistStore(store);