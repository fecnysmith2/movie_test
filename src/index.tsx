import React from 'react';
import ReactDOM from 'react-dom';

/* Import base stylesheet */
import "./assets/styles/index.scss"
/*  Import App component  */
import App from './components/App';

import * as serviceWorker from './serviceWorker';
import {PersistGate} from "redux-persist/integration/react";
import {store, persistor} from './redux/store/store';
import {Provider} from "react-redux";

ReactDOM.render(
    <React.StrictMode>
        <Provider store={store}>
            <PersistGate loading={null} persistor={persistor}>
                <App />
            </PersistGate>
        </Provider>
    </React.StrictMode>,
    document.getElementById('root')
);

serviceWorker.register();
